module consult.service

go 1.18

require github.com/confluentinc/confluent-kafka-go v1.8.2

require github.com/streadway/amqp v1.0.0

require github.com/rs/zerolog v1.26.1
