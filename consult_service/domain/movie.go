package domain

type Movies struct {
	Movies []Movie
}

type Movie struct {
	Title            string
	Poster           string
	ReleaseDate      string
	Genres           []string
	Overview         string
	OriginalLanguage string
}

func NewMovie(title string, poster string, releaseDate string, genres []string, overview string, originalLanguague string) *Movie {
	movie := &Movie{
		Title:            title,
		Poster:           poster,
		ReleaseDate:      releaseDate,
		Genres:           genres,
		Overview:         overview,
		OriginalLanguage: originalLanguague,
	}

	return movie
}
