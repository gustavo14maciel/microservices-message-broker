package main

import (
	"time"

	app "consult.service/app"
	"consult.service/infrastructure/rabbitmq"
	"consult.service/infrastructure/tmdb"
	"github.com/rs/zerolog"
)

func main() {
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	uptimeTicker := time.NewTicker(10 * time.Second)

	r := rabbitmq.NewRabbitMQProducer()
	service := app.NewPublishService(r)

	for range uptimeTicker.C {
		movies := tmdb.GenerateMovies()
		service.Publish(movies)
	}
}
