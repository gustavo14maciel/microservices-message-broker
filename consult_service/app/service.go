package application

import (
	"encoding/json"

	"consult.service/domain"
	"github.com/rs/zerolog/log"
)

type publishService struct {
	messageBroker MessageBroker
}

func NewPublishService(messageBroker MessageBroker) MessageBroker {
	return &publishService{
		messageBroker,
	}
}

func (p *publishService) Publish(movies *domain.Movies) {
	p.messageBroker.Publish(movies)
}

func MarshalMovies(movies *domain.Movies) []byte {
	log.Info().Msg("Marshal movies")

	jsonMovies, err := json.Marshal(movies)

	if err != nil {
		FailOnError(err, "Marshal error")
	}

	log.Info().Msg("Movies in JSON Ready")

	return jsonMovies
}

func CorrectMovieListLen(movies *domain.Movies) *domain.Movies {
	emptyMoviesList := &domain.Movies{}

	if len(movies.Movies) > 5 {
		for i := 0; i < 5; i++ {
			emptyMoviesList.Movies = append(emptyMoviesList.Movies, movies.Movies[i])
		}
	} else {
		return movies
	}

	return emptyMoviesList
}
