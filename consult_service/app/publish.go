package application

import "consult.service/domain"

type PublisherService interface {
	Publish(movies *domain.Movies)
}

type MessageBroker interface {
	Publish(movies *domain.Movies)
}
