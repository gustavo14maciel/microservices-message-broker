package tmdb

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"time"

	error "consult.service/app"
	"consult.service/domain"
	"github.com/rs/zerolog/log"
)

type query struct {
	ApiToken string
	Page     int
	Year     int
	GenreId  string
}

func GenerateMovies() *domain.Movies {
	results := fetchMovies().Results

	movies := &domain.Movies{}

	log.Info().Msg("Generating movies")

	for _, result := range results {
		poster := fmt.Sprintf("https://image.tmdb.org/t/p/original/%s", result.PosterPath)
		movie := domain.NewMovie(result.Title, poster, result.ReleaseDate, generateGenres(result.GenreIds), result.Overview, result.OriginalLanguage)
		movies.Movies = append(movies.Movies, *movie)
	}

	log.Info().Msg("Movies generated")

	return movies
}

func generateGenres(genreIds []int) []string {
	var stringGenres []string
	for _, genreId := range genreIds {

		stringGenres = append(stringGenres, genreCategory[genreId])
	}

	return stringGenres
}

func fetchMovies() *response {
	query := newQuery()

	apiUrl := "https://api.themoviedb.org/3/discover/movie"

	url := fmt.Sprintf("%s?api_key=%s&language=en-US&page=%d&year=%d&with_genres=%s", apiUrl, query.ApiToken, query.Page, query.Year, query.GenreId)

	log.Info().Msg("Making request to TMDB API")

	resp, err := http.Get(url)

	error.FailOnError(err, "Failed to get movies from TMDB")

	body, err := ioutil.ReadAll(resp.Body)

	error.FailOnError(err, "Failed to read response body")

	response := &response{}

	err = json.Unmarshal(body, response)

	error.FailOnError(err, "Failed to unmarshal body")

	log.Info().Msg("Request OK")

	return response
}

func newQuery() *query {
	if os.Getenv("API_TOKEN") == "" {
		log.Fatal().Msg("API_TOKEN env no set")

	}

	year, page := generateQueryFields()

	query := &query{
		ApiToken: os.Getenv("API_TOKEN"),
		Page:     page,
		Year:     year,
		GenreId:  "27",
	}

	return query
}

func generateQueryFields() (int, int) {
	var year, page int

	year = generateYear()
	page = generatePage(year, page)

	return year, page
}

func generateYear() int {
	rand.Seed(time.Now().UnixNano())

	minYear := 1950
	maxYear := 2022

	year := rand.Intn(maxYear-minYear+1) + minYear

	return year
}

func generatePage(year int, page int) int {
	if year >= 1950 && year < 1960 {
		page = 1
	} else if year >= 1960 && year < 1970 {
		page = 4
	} else if year >= 1970 && year < 1980 {
		page = 8
	} else if year >= 1980 && year < 1990 {
		page = 13
	} else if year >= 1990 && year < 2000 {
		page = 15
	} else if year >= 2000 && year < 2010 {
		page = 22
	} else {
		page = 54
	}

	rand.Seed(time.Now().UnixNano())
	randomPage := rand.Intn(page-1+1) + 1

	return randomPage
}
