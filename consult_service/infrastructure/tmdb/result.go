package tmdb

type response struct {
	Results []results `json:"results"`
}

type results struct {
	Id               int    `json:"id"`
	PosterPath       string `json:"poster_path"`
	Overview         string `json:"overview"`
	ReleaseDate      string `json:"release_date"`
	GenreIds         []int  `json:"genre_ids"`
	Title            string `json:"title"`
	OriginalLanguage string `json:"original_language"`
}
