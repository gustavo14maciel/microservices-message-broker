package kafka

import (
	"log"

	app "consult.service/app"
	"consult.service/domain"
	"github.com/confluentinc/confluent-kafka-go/kafka"
)

type kafkaProducer struct {
	Producer *kafka.Producer
	Topic    string
}

func NewKafkaProducer() *kafkaProducer {
	log.Println("===================Creating Kafka Producer===================")

	kafka, err := kafka.NewProducer(&kafka.ConfigMap{
		"bootstrap.servers": "localhost",
	})

	if err != nil {
		log.Fatalln("Error FROM Kafka producer: ", err)
	}

	producer := &kafkaProducer{
		Producer: kafka,
		Topic:    "consult",
	}
	log.Println("===================Kafka Producer Ready===================")
	return producer
}

func (k *kafkaProducer) Publish(movies *domain.Movies) error {
	log.Println("===================Publishing movies in Kafka Topic===================")

	err := k.Producer.Produce(&kafka.Message{
		TopicPartition: kafka.TopicPartition{Topic: &k.Topic, Partition: int32(kafka.PartitionAny)},
		Value:          []byte(app.MarshalMovies(movies)),
	}, nil)

	if err != nil {
		return err
	}

	k.Producer.Flush(15 * 500)

	log.Println("===================Messages published===================")

	return nil
}
