package vault

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"

	error "consult.service/app"
	"github.com/rs/zerolog/log"
)

func GetQueueCredentials() (string, string) {
	log.Info().Msg("Getting credentials from Vault")

	req, err := http.NewRequest("GET", "http://localhost:8200/v1/secret/data/rabbitmq", nil)

	error.FailOnError(err, "Failed to make GET request to Vault")

	req.Header.Set("X-Vault-Token", os.Getenv("VAULT_TOKEN"))

	log.Info().Msg("Sending HTTP request to Vault")
	resp, err := http.DefaultClient.Do(req)

	error.FailOnError(err, "Failed to send HTTP request via Client")

	defer resp.Body.Close()

	log.Info().Msg("Reading response body")
	body, err := ioutil.ReadAll(resp.Body)

	error.FailOnError(err, "Failed to read response body")

	response := &Response{}

	log.Info().Msg("Unmarshal body")
	err = json.Unmarshal(body, response)

	error.FailOnError(err, "Failed to unmarshal body")

	return response.Response.Data["user"], response.Response.Data["password"]

}
