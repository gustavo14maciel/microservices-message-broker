package rabbitmq

import (
	"fmt"

	app "consult.service/app"
	"consult.service/domain"
	"consult.service/infrastructure/vault"
	"github.com/rs/zerolog/log"
	"github.com/streadway/amqp"
)

type rabbitMQProducer struct {
	Connection *amqp.Connection
}

func NewRabbitMQProducer() *rabbitMQProducer {
	user, password := vault.GetQueueCredentials()

	log.Info().Msg("Creating RabbitMQ Producer")

	conn, err := amqp.Dial(fmt.Sprintf("amqp://%s:%s@127.0.0.1:5672/", user, password))

	producer := &rabbitMQProducer{
		Connection: conn,
	}

	app.FailOnError(err, "Failed to open connection to RabbitMQ")

	log.Info().Msg("RabbitMQ Producer ready")
	return producer
}

func (r *rabbitMQProducer) Publish(movies *domain.Movies) {
	correctLenList := app.CorrectMovieListLen(movies)

	log.Info().Msg("Start to publish messages on RabbitMQ")

	ch, err := r.Connection.Channel()

	app.FailOnError(err, "Failed to open RabbitMQ Channel")

	defer ch.Close()

	q, err := ch.QueueDeclare(
		"consult", // name
		true,      // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		nil,       // arguments
	)

	app.FailOnError(err, "Failed to declare Queue")

	err = ch.Publish(
		"",     // exchange
		q.Name, // routing key
		false,  // mandatory
		false,  // immediate
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "application/json",
			Body:         []byte(app.MarshalMovies(correctLenList)),
		})

	app.FailOnError(err, "Failed to publish messages")

	log.Info().Msg("Messages published")
}
