import psycopg2
import os

SCHEMA_NAME = 'microservice'
TB_NAME = 'email'

def connect_to_db():
    try:
        conn = psycopg2.connect(
            host=os.environ['DB_HOST'],
            database=os.environ['DB_NAME'],
            user=os.environ['DB_USER'],
            password=os.environ['DB_PASS'])

        cursor = conn.cursor()

        return cursor, conn
    except (psycopg2.OperationalError, KeyError) as err:
        print("Error on: connect_to_rds")
        print("Some error happened: ", err)

def save_email(to, sent_from, subject, date_sent, content):
    try:
        cursor, conn = connect_to_db()
        values = f"('{to}', '{sent_from}', '{subject}', '{content}', '{date_sent}')"
        query = f'INSERT INTO {SCHEMA_NAME}.{TB_NAME} ("from", "to", subject, "content", date_sent) VALUES {values}'
        cursor.execute(query)
        conn.commit()

        print("Email saved on database")
    except (Exception, psycopg2.DatabaseError) as err:
        print("Some error happened: ", err)
