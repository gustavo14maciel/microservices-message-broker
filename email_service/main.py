import rabbitmq
import vault

def main():
    user, password = vault.get_queue_credentials()
    rabbitmq.start_receiving(user, password)


if __name__ == '__main__':
    try:
        print("Running Email service")
        main()
    except KeyboardInterrupt:
        print('Interrupted')