import smtplib
import ssl
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import os
from datetime import datetime
from db import save_email

def send_email(movies):
    email = os.environ['EMAIL']
    password = os.environ['PASSWORD']
    html = generate_html(movies)

    msg = MIMEMultipart("alternative")
    msg["Subject"] = "Movies of the day"
    msg["From"] = email
    msg["To"] = email

    part = MIMEText(html, "html")
    msg.attach(part)

    context = ssl.create_default_context()

    with smtplib.SMTP_SSL('smtp.gmail.com', 465, context=context) as server:
        server.login(email, password)
        server.sendmail(email, email, msg.as_string())

    date = datetime.today().strftime('%Y-%m-%d %H:%M:%S')

    print("Email sent")

    save_email(msg['To'], msg["From"], msg["Subject"], date, html)

def generate_html(movies):
    titles = str()
    posters = str()

    for movie in movies:
        titles += f'<h1>{movie["title"]}</h1>, '
        posters += f'<img src={movie["poster"]} style="width:300px;height:300px;"><br>'

    html = (f"""\
            <html>
              <body>
                <p><h1>Your movies for the day</h1>
                   {titles[:-2]}
                   <br>{posters}
                </p>
              </body>
            </html>
            """)

    return html