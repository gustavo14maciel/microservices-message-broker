import hvac
import os

def get_queue_credentials():
    try:
        print("Getting credentials from Vault")

        client = hvac.Client(url='http://localhost:8200', token=os.environ['VAULT_TOKEN'])
        secret = client.kv.v2.read_secret(path='rabbitmq')
        data = secret['data']['data']

        print("Credentials ready")

        return data['user'], data['password'],
    except Exception as err:
        print("Error: ", err)