import json
import email_movies
import pika

def start_receiving(user, password):
    try:
        print("Creating RabbitMQ credentials")
        credentials = pika.PlainCredentials(user, password)

        connection = pika.BlockingConnection(pika.ConnectionParameters('localhost', 5672, '/', credentials))
        print("Connected to RabbitMQ")

        channel = connection.channel()

        print("Consuming messages")
        channel.basic_consume(queue='email',
                              auto_ack=True,
                              on_message_callback=callback)

        print(' [*] Waiting for messages...')
        channel.start_consuming()

    except pika.exceptions.ProbableAuthenticationError as err:
        print(err)

def callback(ch, method, properties, body):
    movies = json.loads(body)
    email_movies.send_email(movies['Movies'])