![Architecture using microservices in a diagram](./images/microservices.png)

# Arquitetura de Microsserviços

## Descrição do Sistema

A cada X minutos o serviço de Consult vai fazer um request pro TMDB (The Movie Database API) para buscar 5 filmes de terror aleatórios e publicar na fila do RabbitMQ para o serviço de Movie consumir e salvar aquela lista no banco de dados e disponibilizar em um endpoint. Por fim, o serviço de Movie publica em uma outra fila do RabbitMQ para o serviço de Email consumir os filmes, enviar um email para o usuário com a lista de filmes e salvar esse email em outro banco de dados.

## Componentes

- Mensageria/Fila e comunicação assíncrona com RabbitMQ
- Gerenciamento de secrets com Vault
- API Gateway para comunicação do Client com os microsserviços
- Consult - Serviço feito em Golang
- Movie - Serviço feito em Java/Spring Boot
- Email - Serviço feito em Python
- Instâncias do PostgreSQL
