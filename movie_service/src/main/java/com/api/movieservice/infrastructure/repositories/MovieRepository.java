package com.api.movieservice.infrastructure.repositories;
import com.api.movieservice.domain.Movie;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface MovieRepository extends JpaRepository<Movie, UUID> {
    List<Movie> findByWatched(boolean watched);

    @Modifying
    @Query("update Movie m set m.watched = ?2 where m.id = ?1")
    void setWatchedById(UUID id, boolean watched) throws IllegalArgumentException;
}
