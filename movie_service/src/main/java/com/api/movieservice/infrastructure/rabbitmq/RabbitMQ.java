package com.api.movieservice.infrastructure.rabbitmq;
import com.api.movieservice.application.MovieService;
import com.api.movieservice.application.util.JsonParser;
import com.api.movieservice.domain.Movie;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RabbitMQ {

    @Autowired
    MovieService movieService;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @RabbitListener(queues = {"${consult.queue.name}"})
    private void receive(@Payload String message) {
        System.out.println("New message received");
        List<Movie> movies = JsonParser.parseMoviesFromJson(message);

        if (movies.size() == 0) {
            return;
        }

        movieService.saveMoviesOnDatabase(movies);

        List<Movie> unwatchedMovies = movieService.findByWatched(false);
        send(unwatchedMovies);
    }

    private void send(List<Movie> movies) {
        String queue = "email";
        rabbitTemplate.convertAndSend(queue, JsonParser.parseMoviesToJson(movies));
        System.out.println(String.format("Message sent to '%s' queue", queue));
    }
}
