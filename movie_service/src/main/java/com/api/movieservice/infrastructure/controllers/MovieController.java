package com.api.movieservice.infrastructure.controllers;
import com.api.movieservice.application.MovieService;
import com.api.movieservice.domain.Movie;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/api/v1/movie")
public class MovieController {
    final MovieService movieService;

    public MovieController(MovieService movieService) {
        this.movieService = movieService;
    }

    @GetMapping(value = "/watched")
    public ResponseEntity<List<Movie>> getWatchedMovies() {
        return ResponseEntity.status(HttpStatus.OK).body(movieService.findByWatched(true));
    }

    @PatchMapping(value = "/watched/{id}")
    public ResponseEntity<String> patchWatchedMovie(@PathVariable("id") String id) {
        try{
            movieService.setWatchedById(UUID.fromString(id));
            return ResponseEntity.status(HttpStatus.OK).body("Movie watched!");
        } catch (IllegalArgumentException err) {
            System.out.println(err.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err.getMessage());
        }
    }
}
