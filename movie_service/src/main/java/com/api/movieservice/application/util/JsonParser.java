package com.api.movieservice.application.util;
import com.api.movieservice.domain.Movie;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class JsonParser {

    public static List<Movie> parseMoviesFromJson(String jsonString) {
        try {
            List<Movie> movies = new ArrayList<Movie>();
            JSONObject jsonObject = new JSONObject(jsonString);
            JSONArray jsonMovies = jsonObject.getJSONArray("Movies");

            int n = jsonMovies.length();

            for (int i = 0; i < n; i++) {
                JSONObject jsonMovie = jsonMovies.getJSONObject(i);
                Movie m = new Movie(jsonMovie.getString("Title"), jsonMovie.getString("Poster"), jsonMovie.getString("ReleaseDate"), parseMovieGenres(jsonMovie.getJSONArray("Genres")), jsonMovie.getString("Overview"), jsonMovie.getString("OriginalLanguage"));
                movies.add(m);
            }

            return movies;
        } catch(JSONException exception) {
            System.out.println("Error: " + exception.getMessage());
            return new ArrayList<Movie>();
        }
    }

    public static String parseMovieGenres(JSONArray jsonGenres) {
        StringBuilder genres = new StringBuilder();

        int n = jsonGenres.length();

        for (int i = 0; i < n; i++) {
            String genre = jsonGenres.getString(i);
            genres.append(String.format("%s ",genre));
        }

        return genres.toString();
    }

    public static String parseMoviesToJson(List<Movie> movies) {
        return new JSONObject().put("Movies", movies).toString();
    }
}
