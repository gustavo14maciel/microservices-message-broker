package com.api.movieservice.application;
import com.api.movieservice.domain.Movie;
import com.api.movieservice.infrastructure.repositories.MovieRepository;

import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
public class MovieService {
    final MovieRepository movieRepository;

    public MovieService(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }
    
    @Transactional
    public void saveMoviesOnDatabase(List<Movie> movies) {
        List<Movie> emptyMovieList = new ArrayList<>();
        boolean append = false;

        List<Movie> savedMovies = findAllMovies();

        for (Movie movie : movies) {
            if (savedMovies.size() == 0) {
                this.movieRepository.saveAll(movies);
                System.out.println("Saved movies");
                return;
            }

            for (Movie savedMovie : savedMovies) {
                if (Objects.equals(movie.getTitle(), savedMovie.getTitle())) {
                    System.out.println("Movie already saved: " + savedMovie.toString());
                    append = false;
                    break;
                }
                append = true;
            }

            if (append) {
                emptyMovieList.add(movie);
            }

            append = false;
        }

        this.movieRepository.saveAll(emptyMovieList);
        System.out.println("Saved movies");
    }

    private List<Movie> findAllMovies() {
        return this.movieRepository.findAll();
    }

    public List<Movie> findByWatched(boolean bool) {
        if (!bool) {
            List<Movie> dbList = this.movieRepository.findByWatched(bool);
            List<Movie> list = this.movieRepository.findByWatched(bool).subList(dbList.size()-5, dbList.size());
            return list;
        }
        return this.movieRepository.findByWatched(bool);
    }

    @Transactional
    public void setWatchedById(UUID id) throws IllegalArgumentException {
        this.movieRepository.setWatchedById(id, true);
    }
}
